""" Allgemeine Einstellungen
"   Syntax-Hervorhebung aktivieren
syntax on

"   Syntax-Hervorhebung an dunklen Hintergrund anpassen
set background=dark

"   vi-Kompatibilitaets-Modus deaktivieren
set nocp

"   aktuelle Zeile und Spalte anzeigen
set ruler

"   relative Zeilennummer anzeigen
set relativenumber

"   aktuelle Zeile anzeigen
"       zeigt iVm mit relativenumber die aktuelle Zeile an, statt der 0
set number

"   Statuszeile anzeigen
set laststatus=2

"   Tab-Completion fuer Datei-basierte Aufgaben
set path+=**

"   Vervollstaendigungsoptionen als Menue anzeigen
set wildmenu

"   Clipboard verwenden
set clipboard=unnamed

"   Zeilenumbruch nicht innerhalb eines Wortes
set wrap linebreak

"   Ruecktaste aktivieren
set backspace=2

"   farbiges Zeilenende
"set colorcolumn=80

"   aktuelle Zeile hervorheben
set cursorline

"   leader
let mapleader=","

"   mit Pfeiltasten ueber Anfang/Ende einer Zeile
set whichwrap=b,s,<,>,[,]

""" tmp-Verzeichnis
if !isdirectory($HOME . "/.vim/tmp")
    call mkdir($HOME . "/.vim/tmp", "p", 0700)
endif
set directory=~/.vim/tmp

"   Verzeichnis fuer Wiederherstellungsinformationen
if !isdirectory($HOME . "/.vim/undo")
    call mkdir($HOME . "/.vim/undo", "p", 0700)
endif
set undodir=~/.vim/undo

""" permanentes Rueckgaengigmachen (ab Version 7.3)
"   Undo-File setzen
set undofile

"   Verzeichnis zum Speichern (vorher anlegen)
if !isdirectory($HOME . "/.vim/backup")
        call mkdir($HOME . "/.vim/backup", "p", 0700)
endif
set backupdir=~/.vim/backup

"""   Backup-Dateien vor dem Speichern anlegen
set backup


""" Zeileneinrueckung
"   Darstellen
set cindent
"
"   automatische Zeileneinrueckung
set autoindent

"   intelligente Zeileneinrueckung
set smartindent

"   Tabs durch Leerzeichen ersetzen lassen
set expandtab

"   Tab auf 4 Zeichen setzen
set tabstop=4

"   Anzahl der Leezeichen fuer autoindent
set shiftwidth=4

"   Leerzeichen anzeigen
set list
set listchars=space:·,tab:▸\ ,trail:·

"   Ruecktaste loescht Tab, 4 Leerzeichen
set softtabstop=4

" unterer Abstand beim Scrollen
set scrolloff=10

" Wechsel ins Verzeichnis der editierten Datei
set autochdir

""" Sprachenerkennung aktivieren
filetype plugin indent on

""" Suche und Suchmuster
"   Gross-/Kleinschreibung ignorieren
set ignorecase

"   Gross-/Kleinschreibung nicht ignorieren, bei Grossbuchstaben im Muster
set smartcase

"   Suchergebnisse hervorheben
set hlsearch

"   Hervorhebungen wieder deaktivieren
nmap q :nohlsearch<CR>

"   Ergebnisse beim Tippen anzeigen
set incsearch


"""   Verschluesselungsmethode Blowfish
if v:version > 704 || v:version == 704 && has("patch399")
    set cryptmethod=blowfish2
else
    set cryptmethod=blowfish
endif


""" Cursortasten im Normalmodus deaktivieren
"nnoremap <up> <nop>
"nnoremap <down> <nop>
"nnoremap <left> <nop>
"nnoremap <right> <nop>
"nnoremap j gj
"nnoremap k gk


""" code folding mit Leertaste
set foldenable
set foldlevelstart=10
set foldnestmax=10
nnoremap <space> za
set foldmethod=indent


""" Funktionstasten
"   F1 - keine Hilfe, Zeilennummern (nicht) anzeigen
nnoremap <silent> <F1> :call ToggleNumber()<CR>

"   F2 - automatisches Einruecken deaktivieren
nnoremap <F2> :set invpaste paste?<CR>
set pastetoggle=<F2>
set showmode

"   F3 -


"   F4 - aktuelle Zeile unterstreichen
map <F4> :set cursorline <return>

"   F5 - Leerzeichen am Zeilenende entfernen
nnoremap <silent> <F5> :call <SID>StripTrailingWhitespaces()<CR>

"   F6 - extended


"   F7 - extended


"   F8 - extended


"   F9 - extended


"   F10 - Anzeige der Menueleiste
" reserviert fuer GVim


"   ...automatisch beim Speichern bestimmter Quelldateien entfernen
autocmd BufWritePre *.js,*.html,*.php,*.wiki :call <SID>StripTrailingWhitespaces()
function! <SID>StripTrailingWhitespaces()
    " Preparation: save last search, and cursor position.
    let _s=@/
    let l = line(".")
    let c = col(".")
    " Do the business:
    %s/\s\+$//e
    " Clean up: restore previous search history, and cursor position
    let @/=_s
    call cursor(l, c)
endfunction


""" relativnumber umschalten
function! ToggleNumber()
    if(&relativenumber == 1)
        set norelativenumber
        set nonumber
        set nolist
    else
        set relativenumber
        set number
        set list
    endif
endfunction

""" Standard-Eintraege
if has("autocmd")
   au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif
endif

if has("autocmd")
 filetype indent on
endif

" Uncomment the following to have Vim jump to the last position when
" reopening a file
if has("autocmd")
    au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif
endif

" Function to source only if file exists {
" https://devel.tech/snippets/n/vIIMz8vZ/load-vim-source-files-only-if-they-exist
function! SourceIfExists(file)
  if filereadable(expand(a:file))
    exe 'source' a:file
  endif
endfunction
" }

call SourceIfExists("~/.vim/vimrc.extend")
